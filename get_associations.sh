#!/usr/bin/env bash
# This script export two tabular files with the SLURM associations
#  - associations.tab : account  user
#  - default_account.tab : user  default_account
# It can be cronable and the file can be store and accessible to the status_bars main script


source $STATUS_BAR_CONF_PATH

DATE=$(date +"%Y-%m-%d")
sacctmgr -n -P list association format=account,user | grep -v "|$" | sort -u | sed "s/|/\t/" > $TAB_PATH/associations_${DATE}.tab
ln -sf $TAB_PATH/associations_${DATE}.tab $TAB_PATH/associations.tab

sacctmgr -P -n list user | sed -r "s/\|/\t/g" > $TAB_PATH/default_account_${DATE}.tab
ln -sf  $TAB_PATH/default_account_${DATE}.tab  $TAB_PATH/default_account.tab

