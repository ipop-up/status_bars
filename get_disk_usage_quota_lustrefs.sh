#!/usr/bin/env bash
# This script export one tabular file with the disk usage and quota : disk_usage_quota.tab
# account  usage(GB)  soft_quota(GB)  status_bar
# It can be cronable and the file can be store and accessible to the status_bars main script


SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

DATE=$(date +"%Y-%m-%d")

> $TAB_PATH/disk_usage_quota_${DATE}.tab
for PROJECT in $PROJECT_HOME/*; do
    PROJECT_BASENAME=$(basename $PROJECT)
    GID=$(getent group $PROJECT_BASENAME | cut -f 3 -d ':')

    SIZE=$(lfs quota -g $GID $PROJECT | sed -n "4p")

    USAGE=$(toGB $(echo $SIZE | awk '{print $1}') KB)
    SOFTQUOTA=$(toGB $(echo $SIZE | awk '{print $2}') KB)
    
    STATUS_BAR=$(status_bar $USAGE $SOFTQUOTA)

    echo -e $PROJECT_BASENAME"\t"$USAGE"\t"$SOFTQUOTA"\t"$STATUS_BAR >> $TAB_PATH/disk_usage_quota_${DATE}.tab
done
ln -sf $TAB_PATH/disk_usage_quota_${DATE}.tab $TAB_PATH/disk_usage_quota.tab
