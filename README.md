# Status bars
Display status bars per project that allow user to monitor their resource usage

## Note

So far works with:

- The scheduler
  - Slurm
- The filesystems
  - MooseFS
  - FluidFS
  - LustreFS

## Usage

### Configuration
#### Admin side
A conf file that have to be named `conf.sh` can be generated using the [jinja2 template](conf.sh.j2)

```bash
# Enable or disable all or one of the bars
export STATUS_BAR={{ status_bar_switch }}
export STATUS_BAR_CPU={{ status_bar_cpu_switch }}
export STATUS_BAR_DATA={{ status_bar_data_switch }}

# Project home folder
export PROJECT_HOME={{ project_home|default('/shared/projects/') }}

# Storage folder where the extraction get_* functions will write the tables
export TAB_ABSOLUTE_PATH={{ tab_absolute_path|default('/tmp/') }}

# Size of the bar in number of characters
export BAR_SIZE={{ bar_size|default('20') }}

# Two thresholds for the bar colors
export BAR_COLOR_THRESHOLD1={{ bar_color_threshold1|default('15') }}
export BAR_COLOR_THRESHOLD2={{ bar_color_threshold2|default('18') }}

# For FLUIDFS storage
export STORAGE_SERVER_NAME="{{ storage_server_name|default('') }}"
export STORAGE_SERVER_USER="{{ storage_server_user|default('') }}"
export STORAGE_VOLUME="{{ storage_volume|default('') }}"
```

#### User side
The status bars can be disabled or the user can choose to only display one of the two bars

```bash
echo "STATUS_BAR=0" >> ~/.status_bars.conf.sh
echo "STATUS_BAR_CPU=0" >> ~/.status_bars.conf.sh
echo "STATUS_BAR_DATA=0" >> ~/.status_bars.conf.sh
```

### Cronable get functions

```bash
./get_associations.sh; ./get_cpu_usage_quota.sh; ./get_disk_usage_quota.sh
```

### Script that can be displayed on login

```bash
./status_bars
```

## Results:

The bars are colored in green, yellow and red according to 2 thresholds

![](doc/images/status_bars.png)
