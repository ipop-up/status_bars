# This library contained different function for status_bar scripts

function relatvive_scale {
  USAGE=$1; QUOTA=$2
  echo $(($USAGE*$BAR_SIZE/$QUOTA))
}

function printf_repeat() {
  str=$1
  num=$2
  v=$(printf "%${num}s" "$str")
  echo -n "${v// /$str}"
}

function get_bar_color() {
  HASH=$1
  if [ $HASH -le $BAR_COLOR_THRESHOLD1 ]; then
    echo "32"
  elif [ $HASH -ge $BAR_COLOR_THRESHOLD2 ]; then
    echo "31"
  else
    echo "33"
  fi
}

function status_bar() {
  USAGE=$1; QUOTA=$2
  HASH=$(relatvive_scale $USAGE $QUOTA)
  if [ $HASH -gt $BAR_SIZE ]; then HASH=20; fi
  DASH=$(($BAR_SIZE-$HASH))
  COLOR=$(get_bar_color $HASH)
  echo -n -e "\e[${COLOR}m["
  if [ $HASH -gt 0 ]; then
    printf_repeat "#" $HASH
  fi
  if [ $DASH -gt 0 ]; then
    printf_repeat "-" $DASH
  fi
  echo -e "]\e[0m"
}

function toGB {
  SIZE=$1
  UNIT=$2
  if [ "$UNIT" = "KB" ]; then
    echo $SIZE | awk '{print int($1/1024/1024)}'
  fi
  if [ "$UNIT" = "MB" ]; then
    echo $SIZE | awk '{print int($1/1024)}'
  fi
  if [ "$UNIT" = "GB" ]; then
    echo $SIZE | awk '{print int($1)}'
  fi
  if [ "$UNIT" = "TB" ]; then
    echo $SIZE | awk '{print int($1*1024)}'
  fi
}
